import { Request, Response } from "express"
import { handleHttp } from "../utils/error.handle";

// Obtiene un elemento
const getBlog = (req: Request, res: Response) => {
    try {
        
    } catch (error) {
        handleHttp(res, `ERROR GET_BLOG \n${error}`)
    }
};

// Obtiene todos los elementos
const getBlogs = (req: Request, res: Response) => {
    try {
        
    } catch (error) {
        handleHttp(res, `ERROR GET_BLOGS \n${error}`)
    }
};

// Actualiza un elemento
const updateBlog = (req: Request, res: Response) => {
    try {
        
    } catch (error) {
        handleHttp(res, `ERROR UPDATE_BLOG \n${error}`)
    }
};

// Inserta un elemento
const postBlog = (req: Request, res: Response) => {
    try {
        const { body } = req
        res.send(body);

    } catch (error) {
        handleHttp(res, `ERROR POST_BLOG \n${error}`)
    }
};

// Remueve un elemento
const deleteBlog = (req: Request, res: Response) => {
    try {
        
    } catch (error) {
        handleHttp(res, `ERROR DELETE_BLOG \n${error}`)
    }
};

export { getBlog, getBlogs, updateBlog, postBlog, deleteBlog }