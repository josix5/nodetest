import { Request, Response } from "express"
import { handleHttp } from "../utils/error.handle";
import { insertCar, getCars, getCar, updateCar, deleteCar } from "../services/item.service";

// Obtiene un elemento
const getItem = async ({ params }: Request, res: Response) => {
    try {
        const { id } = params;
        const response = await getCar(id);
        const data = response ? response : 'NO DATA FOUND';

        res.send(data);

    } catch (error) {
        handleHttp(res, `ERROR GET_ITEM`, error)
    }
};

// Obtiene todos los elementos
const getItems = async (req: Request, res: Response) => {
    try {
        const response = await getCars();
        const data = response.length > 0 ? response : 'NO DATA FOUND';

        res.send(data);
    } catch (error) {
        handleHttp(res, `ERROR GET_ITEMS`, error)
    }
};

// Actualiza un elemento
const updateItem = async ({ params, body }: Request, res: Response) => {
    try {
        const { id } = params;
        const responseItem = await updateCar(id, body);

        res.send(responseItem);
    } catch (error) {
        handleHttp(res, `ERROR UPDATE_ITEM`, error)
    }
};

// Inserta un elemento
const postItem = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const responseItem = await insertCar(body);

        res.send(responseItem);

    } catch (error) {
        handleHttp(res, `ERROR POST_ITEM`, error)
    }
};

// Remueve un elemento
const deleteItem = async ({ params }: Request, res: Response) => {
    try {
        const { id } = params;
        const responseItem = await deleteCar(id);

        res.send(responseItem);
    } catch (error) {
        handleHttp(res, `ERROR DELETE_ITEM`, error)
    }
};

export { getItem, getItems, updateItem, postItem, deleteItem }