import { Car } from "../interfaces/car.interface";
import ItemModel from "../models/item.model";

const insertCar = async (item: Car) => {
    const responseItem = await ItemModel.create(item);

    return responseItem;
}

const getCars = async () => {
    const responseItem = await ItemModel.find({});

    return responseItem;
}

const getCar = async (id: string) => {
    const responseItem = await ItemModel.findOne({ _id: id });

    return responseItem;
}

const updateCar = async (id: string, newItem: Car) => {
    const responseItem = await ItemModel.findOneAndUpdate({ _id: id }, newItem, { new: true });

    return responseItem;
}

const deleteCar = async (id: string) => {
    const responseItem = await ItemModel.deleteOne({ _id: id });
    const deletedCount = responseItem.deletedCount;

    if(deletedCount > 0)
        return `Car with ID ${id} was successfully deleted`;
    else
        return `Car with ID ${id} not found`;
}

export { insertCar, getCars, getCar, updateCar, deleteCar }