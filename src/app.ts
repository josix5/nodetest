import "dotenv/config";
import express from "express";
import cors from "cors";
import { router } from './routes';
import db from "./config/mongo";
import swaggerUi from "swagger-ui-express";
import swaggerSpec from "./swagger";

const PORT = process.env.PORT || 3001;
const app = express()

app.use(cors());
app.use(express.json());
app.use(router);

db().then(() => console.log('Database connection successful')).catch(error => console.error(`DB CONNECTION -> An error has ocurred: \n${error}`));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}`);
    //swaggerDocs(app, PORT);
});