import { Request, Response, Router } from "express";
import { deleteItem, getItem, getItems, postItem, updateItem } from "../controllers/item.controller";

/**
 * Link to consume endpoint http://localhost:3002/<endpoint>
 * Swagger documentation api usage link http://localhost:3002/api-docs/<endpoint>
 */
const router = Router()

/**
 * @swagger
 * /item/insertCar/:
 *   post:
 *     summary: Insert a car
 *     description: Add a new car sending a json with the params.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Car'
 *     responses:
 *       200:
 *         description: Car was added.
 *       404:
 *         description: An error has ocurred adding the car.
 * /item/listCars/:
 *   get:
 *     summary: Get all the cars
 *     description: Get the list of all items car
 *     responses:
 *       200:
 *         description: Car list is showed.
 *       404:
 *         description: No elements found.
 * /item/findCar/{id}:
 *   get:
 *     summary: Get a car by id
 *     description: Get the car that you need by its id.
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Id of car element in the list.
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Car was found.
 *       404:
 *         description: Car was not found.
 * /item/updateCar/{id}:
 *   put:
 *     summary: Update a car by id.
 *     description: Get the id and update the car.
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Id of car element in the list.
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Car'
 *     responses:
 *       200:
 *         description: Car was updated.
 *       404:
 *         description: The car was not found and updated.
 * /item/deleteCar/{id}:
 *   delete:
 *     summary: Delete a car by id
 *     description: Get the list of all items car
 *     parameters:
 *       - name: id
 *         in: path
 *         description: Id of car element in the list to delete.
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: The car was deleted.
 *       404:
 *         description: An error has ocurred deleting the car.
 */
router.get('/listCars/', getItems);
router.get('/findCar/:id', getItem);
router.put('/updateCar/:id', updateItem);
router.post('/insertCar/', postItem);
router.delete('/deleteCar/:id', deleteItem);


export { router }