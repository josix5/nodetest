import swaggerJSDoc from "swagger-jsdoc";
import path from "path";


const options: swaggerJSDoc.Options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Josix DevApp Documentation',
      version: '1.0.0'
    },
    components: {
      schemas: {
        Car: {
          type: 'object',
          properties: {
            name: { type: 'string' },
            color: { type: 'string' },
            gas: { type: 'string', enum: ['gasoline', 'electric'] },
            year: { type: 'integer' },
            price: { type: 'number' },
            description: { type: 'string' }
          }
        }
      }
    }
  },
  apis: [`${path.join(__dirname, './routes/*')}`]
}

const swaggerSpec = swaggerJSDoc(options);

export default swaggerSpec;